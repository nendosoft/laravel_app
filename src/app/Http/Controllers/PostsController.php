<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use Storage;

class PostsController extends Controller
{
    public function index()
    {
        // return view('posts.create');

        $posts = Post::all();
        return view('posts.create', ['posts' => $posts]);
    }
  
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:7',
            'image_path' => 'required|image'
        ], [
            'title.required' => 'タイトルを入力して下さい',
            'title.max' => '7文字以内で入力して下さい',
            'image_path.required' => '画像が選択されていません',
            'image_path.image' => '画像ファイルではありません',
        ]);

        $post = new Post;
        // $form = $request->all();

        // // アップロードしたファイル名を取得
        // $upload_name = $_FILES['goods_image']['name'];

        // title
        $post->title = $request->input('title');

        //s3アップロード開始
        $image_path = $request->file('image_path');

        // バケットの`myprefix`フォルダへアップロード
        $path = Storage::disk('s3')->putFile('public', $image_path, 'public');
        // $path = Storage::disk('s3')->putFileAs('public', $image_path, $post->title, 'public');

        // アップロードした画像のフルパスを取得
        $post->image_path = Storage::disk('s3')->url($path);
  
        $post->save();
  
        return redirect('s3laravel');
    }

    public function destroy(Request $request, $post)
    {
        $post = $request->input('post');
        // app('log')->info($post);
        $json = json_decode($post, true);

        // s3から削除
        $pos = strpos($json['image_path'], 'public');
        // $image = mb_substr($json['image_path'], $pos, NULL, 'UTF-8'); 
        $image = substr($json['image_path'], $pos); 
        $disk = Storage::disk('s3')->delete($image);

        // ローカルストレージから削除
        $post = new Post;
        Post::destroy($json['id']);

        return redirect('s3laravel');
    }

}
