<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Access;

class HelloController extends Controller
{
    public function index() {
        $data = ['msg' => 'これはコントローラから渡されたメッセージです'];

        $yyyymm = date("Ym");
        $count = 1;

        // 月アクセス
        $accesses_record = Access::where('yyyymm', $yyyymm)->first();
        if ($accesses_record == null) {
            // 初回
            Access::insert(['yyyymm' => $yyyymm, 'count' => $count]);
        } else {
            // 2回目以降
            $count = $accesses_record->value('count') + 1;
            Access::where('yyyymm', $yyyymm)->update(['count' => $count]);
        }
        $data['count'] = $count;

        // 累計
        $data['total_count'] = Access::sum("count");;

        return view('hello.index', $data);
    }
}
