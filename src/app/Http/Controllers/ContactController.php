<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    // 入力(HTMLバリエーション用)
    public function input_html()
    {
        // Bladeで使う変数
        $hash = array(
            'subtitle' => '入力画面',
        );
        return view('contact/input2')->with($hash);
        // return view('contact/input.html.valid')->with($hash);
    }

    // 入力
    public function input()
    {
        // Bladeで使う変数
        $hash = array(
            'subtitle' => '入力画面',
        );
        return view('contact.input')->with($hash);
    }

    // 確認画面
    public function confirm(\App\Http\Requests\ContactRequest $request)
    {
        // Bladeで使う変数
        $hash = array(
            'subtitle' => '確認画面',
            'request' => $request
        );
        return view('contact.confirm')->with($hash);
    }
    
    // // 完了画面
    // public function finish(Request $request)
    // {
    //     $input = $request->except('action');

    //     return redirect()->action('ContactController@input')->withInput($input);
    // }

}
