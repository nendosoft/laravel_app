<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'type.*' => 'in:サイトについて,運営者について,その他',
            'name' => 'required|max:10',
            'email' => 'required|email',
            'gender' => 'required|in:男,女',
            'body' => 'required|max:1000'
        ];
    }

    public function attributes() {
        return [
            'type' => '種類',
            'name' => 'お名前',
            'email' => 'メールアドレス',
            'gender' => '性別',
            'body' => '内容'
        ];
    }

    public function messages()
    {
        $attributes = $this->attributes();
        return [
            'type.required' => "$attributes[type]は必須項目です。",
            'name.required' => "$attributes[name]は必須項目です。",
            'name.max' => "$attributes[name]は最大10文字です。",
            'email.required' => "$attributes[email]は必須項目です。",
            'email.email' => "$attributes[email]の形式ではありません。",
            'gender.required' => "$attributes[gender]は必須項目です。",
            'body.required' => "$attributes[body]は必須項目です。",

            // "required" => "必須項目です。",
            // "email" => "メールアドレスの形式で入力してください。",
            // "numeric" => "数値で入力してください。",
        ];
    }

}
