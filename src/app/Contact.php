<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'type', 'name', 'email', 'gender', 'body'
    ];
    static $types = [
        'サイトについて', '運営者について', 'その他'
    ];
    static $genders = [
        '男', '女'
    ];
}
