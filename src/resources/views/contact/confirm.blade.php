<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap4 フォームコンポーネント( HTMLバリデーション )</title>

    <!--Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!--Font Awesome5-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

    <!--自作CSS -->
    <style type="text/css">
        <!--
        /*ここに調整CSS記述*/
        -->
    </style>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-light navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Bootstrap4 フォームコンポーネント ( Laravelバリデーション )</a>
</nav>

<!-- Page Content -->
<div class="container mt-5 p-lg-5">

    <section>
        <h1>{{ $subtitle }}</h1>
    </section>

    <section class="bg-light p-5">
    <form action="{{ url('/contact') }}" method="post" class="needs-validation" novalidate>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <table class="table">
            <!--氏名-->
            <tr>
                <th scope="row">名字</th>
                <td>{{ $request->last_name }}</td>
            </tr>
            <tr>
                <th scope="row">名前</th>
                <td>{{ $request->first_name }}</td>
            </tr>
            <input type="hidden" name="last_name" value="{{ $request->last_name }}" />
            <input type="hidden" name="first_name" value="{{ $request->first_name }}" />
            <!--/氏名-->

            <!--Eメール-->
            <tr>
                <th scope="row">Eメール</th>
                <td>{{ $request->mail }}</td>
            </tr>
            <input type="hidden" name="mail" value="{{ $request->mail }}" />
            <!--/Eメール-->

            <!--パスワード-->
            <tr>
                <th scope="row">パスワード</th>
                <td>{{ $request->password }}</td>
            </tr>
            <input type="hidden" name="password" value="{{ $request->password }}" />
            <!--/パスワード-->

            <!--住所-->
            <tr>
                <th scope="row">郵便番号(7桁)</th>
                <td>{{ $request->zip01 }}</td>
            </tr>
            <tr>
                <th scope="row">都道府県</th>
                <td>{{ $request->pref01 }}</td>
            </tr>
            <tr>
                <th scope="row">住所</th>
                <td>{{ $request->addr01 }}</td>
            </tr>
            <input type="hidden" name="last_name" value="{{ $request->zip01 }}" />
            <input type="hidden" name="last_name" value="{{ $request->pref01 }}" />
            <input type="hidden" name="last_name" value="{{ $request->addr01 }}" />
            <!--/住所-->

            <!--性別-->
            <tr>
                <th scope="row">性別</th>
                <td>{{ $request->gender }}</td>
            </tr>
            <input type="hidden" name="last_name" value="{{ $request->gender }}" />
            <!--/性別-->

            <!--スキル-->
            <?php $skill = implode("、",$request->skill); // チェックボックスからの配列を整形 ?>
            <tr>
                <th scope="row">スキル</th>
                <td>{{ $skill }}</td>
            </tr>
            <input type="hidden" name="skill" value="{{ $skill }}" />
            <!--/スキル-->

            <!---備考欄-->
            <tr>
                <th scope="row">備考欄</th>
                <td>{{ $request->remarks }}</td>
            </tr>
            <input type="hidden" name="remarks" value="{{ $request->remarks }}" />
            <!--/-備考欄-->

            <!--利用規約-->
            <tr>
                <th scope="row">利用規約</th>
                <td>{{ $request->terms }}</td>
            </tr>
            <input type="hidden" name="last_name" value="{{ $request->terms }}" />
            <!--/利用規約-->

        </table>

        <!--ボタンブロック-->
        <div class="form-group row">
            <div class="col-sm-12 text-center">
                <button type="button" class="btn btn-info" onClick="history.back()">戻る</button>
                <!--<button type="submit" name="action" value="back" class="btn btn-info">戻るよ</button>-->
                <button type="submit" class="btn btn-info" disabled>送信 (※無効にしています)</button>
            </div>
        </div>
        <!--/ボタンブロック-->

    </form>
    </section>
</div><!-- /container -->

</body>
</html>
