@extends('layouts.base')
@section('title', 'Insert Demo')
 
@section('styles')
<link href="{{ asset('/css/bootstrap3.css') }}" rel="stylesheet">
@endsection
 
@section('content')
<!-- Navigation -->
<nav class="navbar navbar-light navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Demo</a>
</nav>
 
<div class="container" style="margin-top: 40px;">
  <h3>従業員登録画面</h3>
  <p>入力画面 -> 確認画面 -> <span class="label label-danger">完了画面</span></p>
 
  <div class="alert alert-success" role="alert">データベースにデータを挿入しました！</div>
</div>
@endsection
