<!DOCTYPE HTML>
<html lang="ja">
<head>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet"> -->

<title>@yield('title') </title>
<!-- <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}"> -->

<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@yield('styles')

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]--> 
</head>

<?php echo (Request::is('/')) ? '<body class="theme-invert example">' : '<body class="theme-invert">'; ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{ url('/') }}">
    <img src="{{ asset('/images/nd_favicon.png') }}" class="logo-img">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ action('HelloController@index') }}">home</a>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Practice
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ action('ContactController@input_html') }}">HTML Validation</a>
          <a class="dropdown-item" href="{{ url('contact_laravel') }}">Laravel Validation</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('insert.index') }}">Insert Demo</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ url('task') }}">Vue SPA</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="https://nendo-soft.web.app/" target="_blank">React</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('posts.index') }}">S3</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="http://nendosoft.pythonanywhere.com/" target="_blank">django</a>
          </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('contact') }}">お問い合わせ <span class="sr-only">(current)</span></a>
      </li>
      <!--
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
      -->
    </ul>
    <!--
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    -->
  </div>
</nav>

@yield('content')
 
@yield('footer')
 
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

@yield('scripts')
</body>
</html>
