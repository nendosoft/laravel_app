<!DOCTYPE HTML>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>

  <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet"><!-- Loading Bootstrap -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.3.0/css/flat-ui.min.css" rel="stylesheet"><!-- Loading Flat UI -->
  <link href="{{ asset('/css/starter-template.css') }}" rel="stylesheet"><!--Bootstrap theme(Starter)-->
  <link rel="shortcut icon" href="{{ asset('/images/favicon.ico') }}">

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
 
  @yield('styles')
  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet"><!-- FontAwesome -->
 
  <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
</head>
<body>
 
<!--=================================================
Navbar
==================================================-->
 
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
  <!-- スマートフォンサイズで表示されるメニューボタンとテキスト -->
  <div class="navbar-header">
  <!-- タイトルなどのテキスト -->
  <a class="navbar-brand" href="#">@yield('title')</a>
  </div>
  </div>
</nav>
 
<div class="container" style="margin-top: 40px;">
  @yield('content')
</div><!-- /.container -->
 
<footer class="footer">
  <div class="container">
  <p class="text-muted">@yield('title')</p>
  </div>
</footer>
 
<!-- Bootstrap core JavaScript
  ================================================== -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://vjs.zencdn.net/7.8.3/video.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.3.0/js/flat-ui.min.js"></script>

<script src="{{ asset('/js/prettify.js') }}"></script>
<script src="{{ asset('/js/application.js') }}"></script>
 
@yield('scripts')
</body>
</html>
