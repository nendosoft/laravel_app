@extends('layouts.base')
@section('title', 'S3Laravel')
@section('styles')
<!-- Lightbox for Bootstrap CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />
 @endsection

@section('content')
<!-- Navigation -->
<nav class="navbar navbar-light navbar-dark bg-dark">
    <a class="navbar-brand" href="#">S3 アップロード</a>
</nav>

<div class="container" style="margin-top: 40px;">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif

  <form action="{{ action('PostsController@create') }}" method="post" enctype="multipart/form-data">
    <!-- アップロードフォームの作成 -->
    
    <div class="btn-group" id="app1">
      <input type="text"  name="title" class="form-control" placeholder="タイトルを入力して下さい" size="32"  v-model="textInput">
      <span class="searchclear glyphicon glyphicon-remove-circle" v-on:click="deleteTitle"></span>
    </div>
    <input type="file" name="image_path">
    {{ csrf_field() }}
    <br/><br/>
    <input type="submit" value="アップロード">
  </form>
  
  <br/><br/>

  <table class="table  table-striped table-hover table-bordered table-condensed">
  <thead class="thead-dark">
    <tr><th width="20%">title</th><th width="60%">image</th></tr>
  </thead>
  @foreach($posts as $post)
    <tr>
    @if ($post->image_path)
      <!-- タイトルを表示 -->
      <td>{{ $post->title }}</td>
      <!-- 画像を表示 -->
      <td>
        <a href="{{ $post->image_path }}" data-toggle="lightbox" data-title="{{ $post->title }}" data-footer="">
          <img src="{{ $post->image_path }}" alt=""{{ $post->title }}" class="img-fluid my-2 border"  width="20%" height="20%">
        </a>      
      </td>
      <td>
        <div class="card-body">
          <form action="{{ route('posts.destroy', $post) }}" method="POST" id="submitForm">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}

              <input type="hidden" name="post" value="{{$post}}">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">削除</button>
          </form>
        </div>
      </td>
    @endif
    </tr>
  @endforeach
  </table>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">削除確認</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        本当に削除しますか？
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="mdal" id="hideButton">Close</button> -->
        <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
        <button type="submit" class="btn btn-primary" id="submitButton">削除</button>
      </div>
    </div>
  </div>
</div>

</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>

<script type="text/javascript"> 
$(document).on('click', '[data-toggle="lightbox"]', function (event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});

$("#submitButton").click(function() { 
  $("#submitForm").submit(); 
});
</script>
@stop
