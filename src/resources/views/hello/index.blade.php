@extends('layouts.base')
@section('title', 'Hello/Index')

@section('styles')
<style>
h1 { font-size:100pt; text-align:right; color:#eee; margin:-40px 0px -50px 0px;}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
@endsection

@section('content')
<h1>Index</h1>
<p><?php echo $msg; ?></p>
<p><?php echo date("Y年n月j日"); ?></p>

<p>アクセスカウンター</p>
<table class="table table-bordered w-25">
<tr class="table-secondary "><th scope="col">今月(<?php echo date("Y年n月"); ?>)</th><th scope="col">累計</th></tr>
<tr><td><?php echo $count; ?></td><td><?php echo $total_count; ?></td></tr>
</table>
<br/>

<a href="https://bitbucket.org/nendosoft/laravel_app" target="_blank"><i class="fa fa-bitbucket fa-3x"></i>BitBucket</a>

@endsection