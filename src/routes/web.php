<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/{any}', function() { return view('app'); })->where('any', '.*');

// Route::get('hello', 'HelloController@index');
Route::get('hello', 'HelloController@index')->name('hello');

// Route::get('precontact', function () { return view('contack/index');});
Route::view('precontact', 'contack/index');

Route::get('contact_hrml/', 'ContactController@input_html');
Route::get('contact_laravel/', 'ContactController@input');
Route::patch('contact_patch/', 'ContactController@confirm'); # {{ method_field('patch') }}
// Route::post('contact/', 'ContactController@finish'); # 送信 (※無効にしています)

# InserDemo
Route::get('/insert/index', ['uses' => 'InsertDemoController@index', 'as' => 'insert.index']); # 入力画面
Route::post('insert/confirm', ['uses' => 'InsertDemoController@confirm', 'as' => 'insert.confirm']); # 確認画面
Route::post('insert/finish', ['uses' => 'InsertDemoController@finish', 'as' => 'insert.finish']); # 完了画面

# VueDemo
Route::get('task', function() { return view('task.index'); });
// Route::get('task', 'TaskController@index');
// Route::get('task/{any}', 'TaskController@show');
// Route::get('task/{any}', 'TaskController@index')->where('any', '.*');
// Route::get('/vue/{any}', function() {return view('vue.index');})->where('any', '.*');

# S3Laravel
// Route::get('s3laravel', function() { return view('posts.create'); });
Route::get('s3laravel', ['uses' => 'PostsController@index', 'as' => 'posts.index']);
Route::post('s3laravel/create', ['uses' => 'PostsController@create', 'as' => 'posts.create']); # アップロード画面
Route::delete('s3laravel/{post}', ['uses' => 'PostsController@destroy', 'as' => 'posts.destroy']);

# お問い合わせ
Route::get('contact', 'ContactsController@index');
Route::post('contact/confirm', 'ContactsController@confirm');
Route::post('contact/complete', 'ContactsController@complete');

# php artisan --auth で追加された様な
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
