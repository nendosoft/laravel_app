# laravel_vue + react_firebase + etc
コロナ禍で~~暇になり~~時間が有る為・・  
**経験が浅い、又は未経験の知識を今後の為に習得**する事を目的としたサイト.

## 習得対象
1. laravel  
[Laravel学習帳](https://laraweb.net/)  
何はともあれ、このサイト.  
環境構築(AWSデプロイなど含む)から、xxx
<br/>  
参考書：PHPフレームワーク Laravel入門


1. vue  
[Vue.js + LaravelでシンプルなSPA構築チュートリアル：概要編](https://qiita.com/minato-naka/items/2d2def4d66ec88dc3ca2)  
とても分かり易い、このサイト.  
加えて、参考書、何か買おうかな.基礎を固めれば応用効くし.  
xxx

1. react  
[くるみ割り書房 ft. React](https://oukayuka.booth.pm/)  
①りあクト！ TypeScriptで始めるつらくないReact開発 第3.1版【Ⅰ. 言語・環境編】  
②りあクト！ TypeScriptで始めるつらくないReact開発 第3.1版【Ⅱ. React基礎編】  
③りあクト！ TypeScriptで始めるつらくないReact開発 第3.1版【Ⅲ. React応用編】  
④りあクト！ TypeScriptで極める現場のReact開発  
⑤りあクト！ Firebaseで始めるサーバーレスReact開発    

    電子版購入後に紙版が入庫したとの事で・・紙版も購入w  
React16.8以降のhooksに言及等、内容が新しいと思われる。巷で購入可能な参考書は若干古い気が。。

1. firebase    
  [reactとfirebaseのサイト](https://nendo-soft.web.app/)  
  Google提供。上記3. react⑤を参考に。

1. aws  
無料枠(1年)が終了していたよ。

1. docker  
xxx
1. git  
xxx
1. bootstrap  
xxx

## メモ

| No | 対象 | 概要 | 内容 |
| :--- | :---: | :--- | :--- |
| 01 | docker | コマンド群 | aws ec2にインストールする際のコマンド群が理解出来ていない. [Docker公式(Ubuntu用）](https://docs.docker.com/engine/install/ubuntu/)<br>深追いせず進もうと思う.|
| 02 | aws | 初期表示 | ec2にデプロイ、さあ初表示！と思ったら<br>「ErrorException file_put_contents(/var/www/storage/～～Permission Denied」ってエラーが・・<br>権限周りのエラーとの事なので以下コマンドで解決.<br>`chmod -R 777 storage`<br>`chmod -R 777 bootstrap/cache` |
| 03 | vue | ダイアログ | 削除時の確認ダイアログで「削除」ボタン押下してもaxiosのDELETE処理が機能しない.<br>`v-on:click`ではなく`v-confirm`で解決.常識なんだろうな～やっぱり基礎だな. |
| 04 | aws | インスタンスタイプ | VuejsDialogを使うのでec2でも`composer require laravel/ui`でツールインストールを試みたら固まってしまった.<br>色んなgoogle先生に聞いたら「無料枠のt2.microだとswap領域が足りないのでt2.smallに変更」らしい.<br>一瞬だけt2.smallに変更し、ツールインストール成功！後、t2.microに戻す.請求されたら仕方ない. |
| 05 | vscode | markdown | `Command + Shift + P`後、`Markdown: Open Preview to the Side`を入力すると<br>エディタ右にプレビューが表示されて便利. |
| 06 | xxx | xxx | xxx. |